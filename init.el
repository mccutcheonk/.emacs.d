(server-start)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(c-default-style (quote ((c-mode . "awk") (c++-mode . "awk") (java-mode . "java") (awk-mode . "awk") (other . "gnu"))))
 '(column-number-mode t)
 '(cua-mode t nil (cua-base))
 '(custom-enabled-themes (quote (wombat)))
 '(desktop-save (quote ask-if-new))
 '(desktop-save-mode t)
 '(global-auto-revert-mode t)
 '(global-linum-mode t)
 '(inhibit-startup-screen t)
 '(iswitchb-mode t)
 '(js-indent-level 4)
 '(markdown-command "pandoc")
 '(scroll-bar-mode nil)
 '(size-indication-mode t)
 '(tab-always-indent (quote complete))
 '(tab-width 4)
 '(tool-bar-mode nil))

(require 'package)

;; Add the original Emacs Lisp Package Archive
(add-to-list 'package-archives
             '("elpa" . "http://tromey.com/elpa/"))

;; Add the user-contributed repository
(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)


(let ((default-directory "~/.emacs.d/site-lisp/"))
  (normal-top-level-add-to-load-path '("."))
  (normal-top-level-add-subdirs-to-load-path))

(require 'smart-tab)
(global-smart-tab-mode 1)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

(global-set-key [f7] 'compile)
(global-set-key (kbd "C-#") 'next-error)

(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.h$" . c++-mode))

(require 'cmake-mode)

(add-to-list 'auto-mode-alist '("CMakeLists\\.txt$" . cmake-mode))
(add-to-list 'auto-mode-alist '("\\.cmake$" . cmake-mode))

(put 'upcase-region 'disabled nil)


(let ((platform-settings (concat "~/.emacs.d/init-" (symbol-name system-type))))
  (if (file-exists-p platform-settings)
	  (load platform-settings)))
(put 'downcase-region 'disabled nil)

(require 'web-mode)


(when (load "flymake" t)
  (defun flymake-pyflakes-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name))))
      (list "pyflakes" (list local-file))))
  (add-to-list 'flymake-allowed-file-name-masks
               '("\\.py\\'" flymake-pyflakes-init)))

(require 'flymake-node-jshint)
(add-hook 'js-mode-hook (lambda () (flymake-mode 1)))

(require 'markdown-mode)
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(require 'clojure-mode)
(add-to-list 'auto-mode-alist '("\\.clj$" . clojure-mode))

(require 'compile)

(defun compile-rust ()
  (interactive)
  (let ((default-directory (locate-dominating-file buffer-file-name "Cargo.toml"))
        (compile-command "cargo build"))
    (call-interactively 'compile)))

(add-hook 'rust-mode-hook
		  (lambda () (local-set-key [f5] #'compile-rust)))

;(add-hook 'rust-mode-hook
;          (lambda ()
;            (set (make-local-variable 'compile-command)
;    			 (let ((dir (file-relative-name (locate-dominating-file buffer-file-name "Cargo.toml"))))
;                   (message (format "Path to Cargo.toml: %s" dir))
;    			   (format "cargo build --manifest-path %s"
;    					   (concat dir "Cargo.toml"))))))
                   

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(setq-default indicate-buffer-boundaries 'left)

(setq redisplay-dont-pause t
      scroll-margin 1
      scroll-step 1
      scroll-conservatively 10000
      scroll-preserve-screen-position 1)

(setq mouse-wheel-follow-mouse 't)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

